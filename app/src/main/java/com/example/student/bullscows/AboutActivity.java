package com.example.student.bullscows;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by student on 28.04.2017.
 */
public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_window);
    }
}
