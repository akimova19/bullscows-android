package com.example.student.bullscows;

import android.content.*;
import android.view.*;
import android.widget.*;
import java.util.*;

public class Adapter extends ArrayAdapter<Answer>
{
	private static List<Answer> answers = new ArrayList<Answer>();

	public Adapter(Context context, int resource){
		super(context, resource, answers);
	}

	@Override
	public int getCount()
	{
		return answers.size();
	}

	@Override
	public Answer getItem(int p1)
	{
		return answers.get(p1);
	}

	@Override
	public long getItemId(int p1)
	{
		// TODO: Implement this method
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;

		if (v == null) {

			LayoutInflater vi;
			vi = LayoutInflater.from(getContext());
			v = vi.inflate(R.layout.list_item, null);

		}
		TextView originalTv = (TextView) v.findViewById(R.id.originalTxt);
		TextView answerTv = (TextView) v.findViewById(R.id.answerText);
		Answer tmp = answers.get(position);
		originalTv.setText(tmp.original);
		answerTv.setText(tmp.answer);
		return v;
	}

}
