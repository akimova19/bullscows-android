package com.example.student.bullscows;

import java.io.Serializable;
import java.util.*;

public class Game implements Serializable {
	private String formattedNumber;
	private int digitsCount;
	private boolean isFinished;
	private int turnCount;
	
	public void restart(int complexity){
		turnCount = 0;
		isFinished = false;
		digitsCount = complexity;
		while(!generateNum()){}
	}
	
	public Answer userGuess(String guess){
		int bullsCount = 0, cowsCount = 0;
		for (int i = 0; i < digitsCount; i++) {
			for (int j = 0; j < digitsCount; j++) {
				if (formattedNumber.charAt(i) == guess.charAt(j)) {
					if (i == j) {
						bullsCount++;
					} else {
						cowsCount++;
					}
				}
			}
		}
		turnCount++;
		if(bullsCount==digitsCount){
			isFinished = true;
		}
		String answer = bullsCount + "б" + cowsCount + "к";
		return new Answer(guess, answer);
	}
	
	public int getTurnCount(){
		return turnCount;
	}
	
	public boolean isFinished(){
		return isFinished;
	}
	
	private boolean generateNum() {
        Random r = new Random();
        int min = 0;
        String maxNumberString = "";
        for (int i = 0; i < digitsCount; i++) {
            maxNumberString += 9;
        }
        int max = Integer.valueOf(maxNumberString);
        int number = r.nextInt(max - min) + min;
        formattedNumber = String.format("%0" + digitsCount + "d", number);
        if (checkIsRepeated(formattedNumber)) {
            return false;
        }
        return true;
    }
	
	private boolean checkIsRepeated(String numberStr) {
        for (int i = 0; i < digitsCount; i++) {
            for (int j = i + 1; j < digitsCount; j++) {
                if (numberStr.charAt(i) == numberStr.charAt(j)) {
                    return true;
                }
            }
        }
        return false;
    }
	
    public boolean checkInput(String str) {
        if (str.length() != digitsCount) {
            return false;
        }
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        if (checkIsRepeated(str)) {
            return false;
        }
        return true;
    }
	
}
