package com.example.student.bullscows;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    View.OnClickListener bnClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.startgame:
                    Intent intent = new Intent(MainActivity.this, StartActivity.class);
                    startActivity(intent);
                    break;
                case R.id.settings:
                    Intent intent1 = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(intent1);
                    break;
                case R.id.about:
                    Intent intent2 = new Intent(MainActivity.this, AboutActivity.class);
                    startActivity(intent2);
                    break;
                case R.id.statistics:
                    Intent intent3 = new Intent(MainActivity.this, StatisticsActivity.class);
                    startActivity(intent3);
                    break;

            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_window);

        findViewById(R.id.startgame).setOnClickListener(bnClickHandler);
        findViewById(R.id.statistics).setOnClickListener(bnClickHandler);
        findViewById(R.id.settings).setOnClickListener(bnClickHandler);
        findViewById(R.id.about).setOnClickListener(bnClickHandler);
    }
}
