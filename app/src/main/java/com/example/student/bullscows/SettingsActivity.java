package com.example.student.bullscows;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by student on 28.04.2017.
 */

public class SettingsActivity extends PreferenceActivity {
    public static final String KEY_DIFFICULTY = "diffPref";
    public static final String DEFAULT_DIFFICULTY = "4";
    public static final String KEY_RECORDS = "records";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}

