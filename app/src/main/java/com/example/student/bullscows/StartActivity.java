package com.example.student.bullscows;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.preference.PreferenceManager;

/**
 * Created by student on 28.04.2017.
 */
public class StartActivity extends ListActivity {
    private Game game;
    private EditText editText;
    private Adapter adapter;
    private int diff;
    private final static int RECORDS_COUNT = 10;
    private final static String KEY_GAME = "GAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_window);
        editText = (EditText) findViewById(R.id.numberEditText);
        adapter = new Adapter(this, R.layout.list_item);
        getListView().setAdapter(adapter);
        game = new Game();
        diff = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.KEY_DIFFICULTY, SettingsActivity.DEFAULT_DIFFICULTY));
        game.restart(diff);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_GAME, game);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        game = (Game) state.getSerializable(KEY_GAME);
    }

    public void restartButton(View v) {
        editText.setText("");
        adapter.clear();
        game.restart(diff);
    }

    public void okButton(View v) {
        if (game.isFinished()) {
            return;
        }
        String tmp = editText.getText().toString();
        if (game.checkInput(tmp)) {
            adapter.add(game.userGuess(tmp));
            editText.setText("");
        } else {
            Toast.makeText(this, R.string.input_error, Toast.LENGTH_SHORT).show();
        }
        if (game.isFinished()) {
            Toast.makeText(this, R.string.win, Toast.LENGTH_SHORT).show();
            saveRecord();
        }
    }

    public void saveRecord() {
        int turnCount = game.getTurnCount();
        String[] records = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.KEY_RECORDS + diff, "\n").split("\n");
        int j = 0;
        int newSize = records.length;
        String[] newRecords = new String[RECORDS_COUNT];
        for (int i = 0; i < RECORDS_COUNT; i++) {
            newRecords[i] = "0";
        }
        if (newSize == 0) {
            newRecords[0] = turnCount + "";
        }
        for (int i = 0; i < RECORDS_COUNT; i++) {
            if (i >= newSize) {
                break;
            }
            if (turnCount < Integer.valueOf(records[i])) {
                newRecords[i] = turnCount + "";
                newRecords[i + 1] = records[0];
                i++;
                newSize++;
            } else if (Integer.valueOf(records[i]) == 0) {
                newRecords[i] = turnCount + "";
                break;
            } else {
                newRecords[i] = records[j];
            }
            j++;
        }
        StringBuilder result = new StringBuilder();
        for (String str : newRecords) {
            result.append(str).append("\n");
        }
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString(SettingsActivity.KEY_RECORDS + diff, result.toString()).apply();
    }
}
