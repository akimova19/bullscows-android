package com.example.student.bullscows;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

/**
 * Created by student on 28.04.2017.
 */
public class StatisticsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_window);
        String diff = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.KEY_DIFFICULTY, SettingsActivity.DEFAULT_DIFFICULTY);
        String recordsStr = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.KEY_RECORDS + diff, "").replace("0", "");
        TextView tv = (TextView) findViewById(R.id.statsTextView);
        tv.setText(recordsStr);
    }
}
